import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class SnakeTest {

    Snake peter;
    Snake takis;

	@BeforeEach
	public void setUp() throws Exception {
        peter = new Snake("Peter", 10, "coffee");
        takis = new Snake("Takis", 80, "vegetables");
	}

	@AfterEach
	public void tearDown() throws Exception {

	}


    @Test
    public void testTC1(){
        assertEquals(true, takis.isHealthy());
        assertEquals(false, peter.isHealthy());
    }

    @Test
    public void testTC2(){
	    assertEquals(false, takis.fitsInCage(40));
        assertEquals(false, takis.fitsInCage(80));
        assertEquals(true, takis.fitsInCage(100));
    }

}
