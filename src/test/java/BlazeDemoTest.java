import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.json.Json;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class BlazeDemoTest {

	WebDriver driver;
    String baseUrl = "http://blazedemo.com/";

	@BeforeEach
	public void setUp() throws Exception {

		System.setProperty("webdriver.chrome.driver","/Users/gisetavares/Downloads/chromedriver");
		driver = new ChromeDriver();
		driver.get(baseUrl);
	}

	@AfterEach
	public void tearDown() throws Exception {
		Thread.sleep(5000); // pause 5 sec to close the browser
		driver.close();
	}

	@Test
	public void testTC1(){
		// check if there is 7 departure

		Select departures = new Select(driver.findElement(By.name("fromPort")));
		List<WebElement> numDepartures = departures.getOptions();
		assertEquals(7, numDepartures.size());
	}

	@Test
	public void testTC2(){
		// Virgin America flight #12 Details

		WebElement findFlightsBtn = driver.findElement(By.tagName("input"));
		findFlightsBtn.click();

		WebElement flightTable = driver.findElement(By.className("table"));
		List<WebElement> flightTableRows = flightTable.findElements(By.tagName("tr"));
		String flightContent = flightTableRows.get(4).getText();

		String flightNumberExpected = "12";
		String airlinesExpected = "Virgin America";
		String departExpected = "11:23 AM";
		String arrivesExpected = "1:45 PM";
		String costExpected = "$765.32";

		assertTrue(flightContent.contains(flightNumberExpected));
		assertTrue(flightContent.contains(airlinesExpected));
		assertTrue(flightContent.contains(departExpected));
		assertTrue(flightContent.contains(arrivesExpected));
		assertTrue(flightContent.contains(costExpected));
	}

	@Test
	public void testTC3(){
		WebElement findFlightsBtn = driver.findElement(By.tagName("input"));
		findFlightsBtn.click();

		WebElement flightTable = driver.findElement(By.className("table"));
		List<WebElement> flightTableRows = flightTable.findElements(By.tagName("tr"));
		WebElement flightButton = flightTableRows.get(4).findElement(By.tagName("input"));
		flightButton.click();
		
		WebElement confirmationButton = driver.findElement(By.className("btn-primary"));
		confirmationButton.click();
	}

//
//	@Test
//	public void test() {
//		fail("Not yet implemented");
//	}

}
